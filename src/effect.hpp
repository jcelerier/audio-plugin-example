#pragma once
#include "vst-compat.hpp"

#include <iostream>

template <typename T>
struct Effect : AEffect
{
  audioMasterCallback master{};
  int sampleRate{};
  int bufferSize{};

  explicit Effect(audioMasterCallback master) : master{master}
  {
    AEffect::magic = kEffectMagic;
    AEffect::dispatcher =
        [](AEffect* effect, int32_t opcode, int32_t index, intptr_t value, void* ptr, float opt) {
          return static_cast<T*>(effect)->dispatch(opcode, index, value, ptr, opt);
        };
    AEffect::process = [](AEffect* effect, float** inputs, float** outputs, int32_t sampleFrames) {
      return static_cast<T*>(effect)->process(inputs, outputs, sampleFrames);
    };
    AEffect::processReplacing
        = [](AEffect* effect, float** inputs, float** outputs, int32_t sampleFrames) {
            return static_cast<T*>(effect)->process(inputs, outputs, sampleFrames);
          };
    AEffect::processDoubleReplacing
        = [](AEffect* effect, double** inputs, double** outputs, int32_t sampleFrames) {
            return static_cast<T*>(effect)->processDouble(inputs, outputs, sampleFrames);
          };
    AEffect::setParameter = [](AEffect* effect, int32_t index, float parameter) {
      return static_cast<T*>(effect)->setParameter(index, parameter);
    };
    AEffect::getParameter = [](AEffect* effect, int32_t index) {
      return static_cast<T*>(effect)->getParameter(index);
    };
    AEffect::numPrograms = 0;
    AEffect::numInputs = 1;
    AEffect::numOutputs = 1;
    AEffect::flags = effFlagsCanReplacing | effFlagsCanDoubleReplacing;
    AEffect::resvd1 = 0;
    AEffect::resvd2 = 0;
    AEffect::initialDelay = 0.;
    AEffect::realQualities = 0.;
    AEffect::offQualities = 0.;
    AEffect::ioRatio = 1.;
    AEffect::object = nullptr;
    AEffect::user = nullptr;
    AEffect::uniqueID = 1234;
    AEffect::version = 1;

    sampleRate = master(this, audioMasterGetSampleRate, 0, 0, nullptr, 0.f);
    bufferSize = master(this, audioMasterGetBlockSize, 0, 0, nullptr, 0.f);
    std::cout << sampleRate << " " << bufferSize << std::endl;
  }

  intptr_t dispatch(int32_t opcode, int32_t index, intptr_t value, void* ptr, float opt)
  {
    switch (opcode)
    {
      case effGetEffectName:
        strncpy((char*)ptr, T::effectName, 128);
        break;
      case effGetVendorString:
        strncpy((char*)ptr, T::vendor, 128);
        break;
      case effGetProductString:
        strncpy((char*)ptr, T::product, 128);
        break;
      case effGetVendorVersion:
        strncpy((char*)ptr, T::version, 128);
        break;
    }
    return 0;
  }
};
