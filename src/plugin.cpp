#include "effect.hpp"

#include <my_audio_plugin_export.h>

#include <array>
#include <atomic>

struct MyEffect : public Effect<MyEffect>
{
  static constexpr const char* effectName = "My Gain";
  static constexpr const char* vendor = "ENSEIRB";
  static constexpr const char* product = "Some product";
  static constexpr const char* version = "1.0";

  struct
  {
    constexpr int size() noexcept { return 1; }
    std::atomic<float> gain{};
  } parameters;

  explicit MyEffect(audioMasterCallback cb) : Effect<MyEffect>{cb}
  {
    AEffect::numParams = parameters.size();
  }

  void process(float** inputs, float** outputs, int32_t sampleFrames)
  {
    const float gain = parameters.gain;
    for (int c = 0; c < 2; c++)
      for (int i = 0; i < sampleFrames; i++)
        outputs[c][i] = inputs[c][i] * gain;
  }

  void processDouble(double** inputs, double** outputs, int32_t sampleFrames)
  {
    const double gain = parameters.gain;
    for (int c = 0; c < 2; c++)
      for (int i = 0; i < sampleFrames; i++)
        outputs[c][i] = inputs[c][i] * gain;
  }

  void setParameter(int32_t index, float parameter)
  {
    switch (index)
    {
      case 0:
        parameters.gain = parameter;
        break;
    }
  }

  float getParameter(int32_t index)
  {
    switch (index)
    {
      case 0:
        return parameters.gain;
      default:
        return 0.f;
    }
  }
};

extern "C" MY_AUDIO_PLUGIN_EXPORT AEffect* PluginMain(audioMasterCallback audioMaster)
{
  return new MyEffect{audioMaster};
}